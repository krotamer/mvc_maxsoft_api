﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NextGenerationServer.Entities;
using MvcNextGenertionApi.Services;


namespace MvcNextGenertionApi.Controllers
{
    /// <summary>
    /// Inventory Api Controller
    /// </summary>
    public class InventoryController : ApiController
    {

        private InventoryRepository inventoryRepository;

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public InventoryController()
        {
            inventoryRepository = new InventoryRepository();
        }

        /// <summary>
        /// Function To Get Inventory For Item
        /// <param name="stItem">Item</param>
        /// </summary>        
        /// <returns>ReturnDoubleAnswer</returns>
        [HttpGet]
        [ActionName("getInventoryToItem")]
        public ReturnDoubleAnswer getInventoryToItem(string stItem)
        {

            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }
            return inventoryRepository.getInventoryToItem(stToken, stItem);
        }

        /// <summary>
        /// Function To Get Inventory For Storeroom
        /// <param name="stStoreroom">Machsan</param>
        /// </summary>        
        /// <returns>ReturnDoubleAnswer</returns>
        [HttpGet]
        [ActionName("getInventoryToStoreroom")]
        public ReturnDoubleAnswer getInventoryToStoreroom(string stStoreroom)
        {

            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }
            return inventoryRepository.getInventoryToStoreroom(stToken, stStoreroom);
        }

        /// <summary>
        /// Function To Get list of items in storeroom and there Inventory
        /// <param name="stStoreroom">Machsan</param>
        /// </summary>        
        /// <returns>Inventory Entitie List</returns>
        [HttpGet]
        [ActionName("getInventoryListToStoreroom")]
        public List<Inventory> getInventoryListToStoreroom(string stStoreroom)
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }

            return inventoryRepository.getInventoryListToStoreroom(stToken, stStoreroom,false,false);
        }

        /// <summary>
        /// Function To Get list of items in storeroom with itur and atzva and there Inventory
        /// <param name="stStoreroom">Machsan</param>
        /// </summary>        
        /// <returns>Inventory Entitie List</returns>
        [HttpGet]
        [ActionName("getInventoryListToStoreroomWithIturAndAtzva")]
        public List<Inventory> getInventoryListToStoreroomWithIturAndAtzva(string stStoreroom)
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }

            return inventoryRepository.getInventoryListToStoreroom(stToken, stStoreroom,true,true);
        }

        /// <summary>
        /// Function To Get list of items in storeroom with itur and there Inventory
        /// <param name="stStoreroom">Machsan</param>
        /// </summary>        
        /// <returns>Inventory Entitie List</returns>
        [HttpGet]
        [ActionName("getInventoryListToStoreroomWithItur")]
        public List<Inventory> getInventoryListToStoreroomWithItur(string stStoreroom)
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }

            return inventoryRepository.getInventoryListToStoreroom(stToken, stStoreroom, true, false);
        }
        
        /// <summary>
        /// Function To Get list of items in storeroom with atzva and there Inventory
        /// <param name="stStoreroom">Machsan</param>
        /// </summary>        
        /// <returns>Inventory Entitie List</returns>
        [HttpGet]
        [ActionName("getInventoryListToStoreroomWithAtzva")]
        public List<Inventory> getInventoryListToStoreroomWithAtzva(string stStoreroom)
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }

            return inventoryRepository.getInventoryListToStoreroom(stToken, stStoreroom, false, true);
        }

        /// <summary>
        /// Function To Get list of items in storeroom with atzva and there Inventory
        /// <param name="stToken">Token</param>
        /// <param name="stStoreroom">Machsan</param>
        /// </summary>        
        /// <returns>Inventory Entitie List</returns>
        [HttpGet]
        [ActionName("getInventoryListToStoreroomWithAtzvas")]
        public List<Inventory> getInventoryListToStoreroomWithAtzvas(string stToken,string stStoreroom)
        {
            return inventoryRepository.getInventoryListToStoreroom(stToken, stStoreroom, false, true);
        }

        /// <summary>
        /// Function To Get list tnout mlai
        /// <param name="stToken">Token</param>
        /// <param name="stStoreroom">Machsan</param>
        /// </summary>        
        /// <returns>Inventory Entitie List</returns>
        [HttpGet]
        [ActionName("getTnoutMlaiToStoreroom")]
        public List<TnoutMlai> getTnoutMlaiToStoreroom(string stToken, string stStoreroom)
        {
            return inventoryRepository.getTnoutMlaiToMachsan(stToken, stStoreroom);
        }
    }
}
