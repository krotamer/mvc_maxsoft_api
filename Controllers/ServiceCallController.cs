﻿using MvcNextGenertionApi.Models;
using MvcNextGenertionApi.Services;
using NextGenerationServer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcNextGenertionApi.Controllers
{
    /// <summary>
    /// Service Call Api Controller
    /// </summary>
    public class ServiceCallController : ApiController
    {
        private ServiceCallRepository serviceCallRepository;

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public ServiceCallController()
        {
            this.serviceCallRepository = new ServiceCallRepository();
        }


        /// <summary>
        /// Get Service Call by Service Call Id send by client
        /// </summary>
        /// <param name="stServiceCallId">Service Call Id -- >  Moked - kriat_sherut</param>
        /// <returns>List ServiceCall Entitie</returns>
        [HttpGet]
         public List<ServiceCall> GetServiceCall(string stServiceCallId)
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }
            
            return serviceCallRepository.getServiceCall(stToken,stServiceCallId,new List<KeyValuePair<string,object>>());
        }

        /// <summary>
        /// Return List of all Service Call
        /// </summary>
        /// <returns>List ServiceCall Entitie</returns>
        [HttpGet]
        public List<ServiceCall> GetAll()
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }

            return serviceCallRepository.getServiceCall(stToken,"", new List<KeyValuePair<string, object>>());
        }

        /// <summary>
        /// Return open Service Call List
        /// </summary>
        /// <returns>List ServiceCall Entitie</returns>
        [HttpGet]
        public List<ServiceCall> GetOpenServiceCallList()
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }
            List<KeyValuePair<string, object>> li = new List<KeyValuePair<string, object>>();
            KeyValuePair<string, object> kvp = new KeyValuePair<string,object>("stat","05");
            li.Add(kvp);

            return serviceCallRepository.getServiceCall(stToken, "",li);
        }

        /// <summary>
        /// Return History Service Call List
        /// </summary>
        /// <returns>List ServiceCall Entitie</returns>
        [HttpGet]
        public List<ServiceCall> GetHistoryServiceCallList()
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }
            List<KeyValuePair<string, object>> li = new List<KeyValuePair<string, object>>();
            KeyValuePair<string, object> kvp = new KeyValuePair<string, object>("stat", "15");
            li.Add(kvp);

            return serviceCallRepository.getServiceCall(stToken, "", li);
        }

        /// <summary>
        /// Function To Get Care Description Iturit DataSourse
        /// </summary>        
        /// <returns>IturitDataSource Entitie List</returns>
        [HttpGet]
        public List<IturitDataSource> getCareDescriptionIturitDataSourse()
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }

            return serviceCallRepository.getCareDescriptionIturitDataSourse(stToken);
        }


        /// <summary>
        /// Function To Insert Service Call Treatment
        /// </summary>
        /// <param name="liServiceCallTreatment">ServiceCall Treatment List</param>
        /// <param name="liServiceCallItems">ServiceCall Items List</param>
        /// <param name="liServiceCallItemsPhoto">ServiceCall Items Photo</param>
        /// <param name="serviceCallSignature">serviceCall Signature Entitie</param>
        /// <returns>ReturnAnswer Entitie</returns>
        [HttpPost]
        public ReturnAnswer insertServiceCallTreatment([FromBody] List<ServiceCallTreatment> liServiceCallTreatment, [FromBody] List<ServiceCallItems> liServiceCallItems, [FromBody] List<ServiceCallItemsPhoto> liServiceCallItemsPhoto, [FromBody] ServiceCallSignature serviceCallSignature)
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }
            return serviceCallRepository.insertServiceCallTreatment(stToken, liServiceCallTreatment, liServiceCallItems, liServiceCallItemsPhoto, serviceCallSignature);
        }

        
    }
}
