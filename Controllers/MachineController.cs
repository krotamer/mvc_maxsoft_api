﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NextGenerationServer.Entities;
using MvcNextGenertionApi.Services;

namespace MvcNextGenertionApi.Controllers
{
    /// <summary>
    /// Machine Api Controller
    /// </summary>
    public class MachineController : ApiController
    {
        /// <summary>
        /// Instanse of Machine Repository
        /// </summary>
        private MachineRepository machineRepository;

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public MachineController()
        {
            machineRepository = new MachineRepository();
        }
        
        /// <summary>
        /// Function To Get the List Of Machines
        /// </summary>        
        /// <returns>Machine</returns>
        [HttpGet] 
        public List<Machine> Get()
        {

            IEnumerable<string> headerValues;
            string stToken=string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }
            return machineRepository.getMachineList(stToken);
        }

    }
}
