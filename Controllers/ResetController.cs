﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MvcNextGenertionApi.Models;
using MvcNextGenertionApi.Services;
using NextGenerationServer.Entities;

namespace MvcNextGenertionApi.Controllers
{
    /// <summary>
    /// Reset Password Api Controller
    /// </summary>
    public class ResetController : ApiController
    {
        private LoginRepository loginRepository;

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public ResetController()
        {
            this.loginRepository = new LoginRepository();
        }

        /// <summary>
        /// Reset Password Post Action
        /// </summary>
        /// <param name="resetPwd">Gets User name</param>
        /// <returns>String Result</returns>
        [HttpPost]             
        public ReturnAnswer Post([FromBody] ResetPassword resetPwd)
        {
            //[ActionName("ResetPassword")]   
            return loginRepository.resetPassword(resetPwd);
        }

    }
}
