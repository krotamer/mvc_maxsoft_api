﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MvcNextGenertionApi.Models;
using MvcNextGenertionApi.Services;
using NextGenerationServer.Entities;

namespace MvcNextGenertionApi.Controllers
{
    
    /// <summary>
    /// Login Api Controller
    /// </summary>
    public class LoginController : ApiController
    {
        private LoginRepository loginRepository;

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public LoginController()
        {
            this.loginRepository = new LoginRepository();
        }

        /// <summary>
        /// Login Post Action
        /// </summary>
        /// <param name="login">Login Model - username,password</param>
        /// <returns>User Model</returns>        
        [HttpPost]       
        public User Post([FromBody] Login login)
        {
            //[ActionName("CheckUser")]
            return loginRepository.checkUser(login);
        }

        /// <summary>
        /// Check If User Is Valid and already loged in
        /// </summary>
        /// <returns>User Model</returns>
        [HttpGet]
        [ActionName("login")]
        public User login(string stUser, string stPwd)
        {

            return loginRepository.checkUser(stUser, stPwd);
        }
/*
        /// <summary>
        /// Check If User Is Valid and already loged in
        /// </summary>
        /// <returns>User Model</returns>
        [HttpGet]
        public User Get()
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }
            return loginRepository.checkUser(stToken);
        }
 * 
 */
    }
}
