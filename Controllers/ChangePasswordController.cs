﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MvcNextGenertionApi.Models;
using MvcNextGenertionApi.Services;
using NextGenerationServer.Entities;

namespace MvcNextGenertionApi.Controllers
{
    /// <summary>
    /// ChangePasswordController Api Controller
    /// </summary>
    public class ChangePasswordController : ApiController
    {
        private LoginRepository loginRepository;

        /// <summary>
        ///  Empty Constructor
        /// </summary>
        public ChangePasswordController()
        {
            this.loginRepository = new LoginRepository();
        }
        
        /// <summary>
        /// Function To Change Password
        /// </summary>
        /// <param name="newPassword">NewPassword model - user_name,old_pwd,new_pwd</param>
        /// <returns>True If Succes else return false</returns>
        [HttpPost]
        public ReturnAnswer Post([FromBody] NewPassword newPassword)
        {
            //[ActionName("ChangePwd")]
            return loginRepository.changePassword(newPassword);
        }


    }
}
