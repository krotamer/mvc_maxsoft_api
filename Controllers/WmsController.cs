﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NextGenerationServer.Entities;
using MvcNextGenertionApi.Services;

namespace MvcNextGenertionApi.Controllers
{
    public class WmsController :  ApiController
    {
        private WmsRepository wmsRepository ;

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public WmsController()
        {
            wmsRepository = new WmsRepository();
        }

        /// <summary>
        /// get Orders to Shipment
        /// </summary>
        /// <param name="stShipment">Shipment</param>
        /// <returns>List Of orders for Shipment</returns>
        [HttpGet]
        [ActionName("getOrdersToShipment")]
        public List<Est169> getOrdersToShipment(string stShipment)
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }
            return wmsRepository.getOrdersToShipment(stToken, stShipment);
        }

        /// <summary>
        /// get Orders Details
        /// </summary>
        /// <param name="stOrderMoked">Hazmana Moked</param>
        /// <param name="stOrderTeuda">Hazmana Teuda</param>
        /// <returns>Orders Details</returns>
        [HttpGet]
        [ActionName("getOrdersDetails")]
        public Est169 getOrdersDetails(string stOrderMoked, string stOrderTeuda)
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }
            return wmsRepository.getOrdersDetails(stToken, stOrderMoked, stOrderTeuda);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stOrderMoked"></param>
        /// <param name="stOrderTeuda"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("rePrintTeudatMishloach")]
        public bool rePrintTeudatMishloach(string stOrderMoked, string stOrderTeuda)
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }
            return wmsRepository.rePrintTeudatMishloach(stToken, stOrderMoked, stOrderTeuda);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stShipment"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("rePrintTeudatMishloachForShipment")]
        public bool rePrintTeudatMishloachForShipment(string stShipment)
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }
            return wmsRepository.rePrintTeudatMishloachForShipment(stToken, stShipment);
        }




    }
}
