﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NextGenerationServer.Entities;
using MvcNextGenertionApi.Services;
using MvcNextGenertionApi.Models;

namespace MvcNextGenertionApi.Controllers
{
    public class StoreroomController : ApiController
    {
        private StoreroomRepository storeroomRepository;

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public StoreroomController()
        {
            storeroomRepository = new StoreroomRepository();
        }

        /// <summary>
        /// function to get the list of storeroom for user
        /// </summary>
        /// <returns>list of store rooms</returns>
        [HttpGet]
        public List<StoreRoom> getListOfStorerooms()
        {
            IEnumerable<string> headerValues;
            string stToken = string.Empty;
            if (Request.Headers.TryGetValues("Token", out headerValues))
            {
                stToken = headerValues.First();
            }

            return storeroomRepository.getListOfStorerooms(stToken);
        }

        /// <summary>
        /// function to get the list of storeroom for user
        /// </summary>
        /// <param name="stToken">Token</param>
        /// <returns>list of store rooms</returns>
        [HttpGet]
        public List<TableRec> getListOfStoreroomss(string stToken)
        {
            List<StoreRoom> liB =  storeroomRepository.getListOfStorerooms(stToken,"sug_shiyuch DESC");
            if (liB == null)
                return null;
            List<StoreRoom> li = new List<StoreRoom>();

            foreach (StoreRoom storeRoom in liB)
            {
                if (string.IsNullOrEmpty(storeRoom.Sug_shiyuch) == false)
                {
                    if ((storeRoom.Sug_shiyuch.Trim().CompareTo("דגירה") == 0) || (storeRoom.Sug_shiyuch.Trim().CompareTo("בקיעה") == 0))
                        li.Add(storeRoom);
                }
            }
            if (li == null)
                return null;
            
            InventoryRepository inventoryRepository = new InventoryRepository();
            List<TableRec> lt = new List<TableRec>();
            List<TnoutMlai> lTm = new List<TnoutMlai>();
            bool bIsFirst = true;            

            TableRec tr = new TableRec();
            int j = 0;
            for (int i = 0; i < li.Count; i++)
            {
                if ( i % 8 == 0)
                    tr = new TableRec();
                j = i % 8 + 1;

                if (j == 1)
                {
                    tr.Valid1 = true;
                    tr.StoreRoomName1 = li[i].Teur;
                    tr.StoreRoomCode1 = li[i].Machsan;
                    tr.StoreRoomKibolet1 = li[i].Kibolet;
                    tr.StoreRoomInventory1 = inventoryRepository.getInventoryToStoreroom(stToken,li[i].Machsan).Answer;
                    tr.StoreRoomNumYamim1 = li[i].Ifyun_dec_1;
                    tr.Type1 = li[i].Sug_shiyuch;
                    lTm = NextGenerationServer.BLL.AccessBLL.Default.Inventory.getTnoutMlaiToMachsan(stToken, li[i].Machsan);
                    tr.IsSingal1 = true;
                    bIsFirst = true; 
                    foreach (TnoutMlai item in lTm)
                    {
                        if ((item.Kod_tnua.Trim().CompareTo("611") == 0) && (item.Kamut_rashit > 0))
                        {
                            if (bIsFirst)
                            {                                
                                tr.Leaka1 = item.Atzva.Substring(0, 3);
                                tr.DateKnisa1 = item.D_erua;
                                tr.DaysLeft1 = item.Yamim_notar;
                                tr.MlayEggs1 = item.Kamut_rashit;
                                bIsFirst = false; 
                            }
                            else
                            {
                                if (item.D_erua.CompareTo(tr.DateKnisa1) == 0)
                                {
                                    tr.MlayEggs1 += item.Kamut_rashit;
                                }
                                else
                                {
                                    tr.IsSingal1 = false;
                                }
                            }
                        }
                    }
                    if (bIsFirst)
                        tr.IsSingal1 = false;
                }
                else if (j == 2)
                {
                    tr.Valid2 = true;
                    tr.StoreRoomName2 = li[i].Teur;
                    tr.StoreRoomCode2 = li[i].Machsan;
                    tr.StoreRoomKibolet2 = li[i].Kibolet;
                    tr.StoreRoomInventory2 = inventoryRepository.getInventoryToStoreroom(stToken, li[i].Machsan).Answer;
                    tr.StoreRoomNumYamim2 = li[i].Ifyun_dec_1;
                    tr.Type2 = li[i].Sug_shiyuch;
                    lTm = NextGenerationServer.BLL.AccessBLL.Default.Inventory.getTnoutMlaiToMachsan(stToken, li[i].Machsan);
                    tr.IsSingal2 = true;
                    bIsFirst = true; 
                    foreach (TnoutMlai item in lTm)
                    {
                        if ((item.Kod_tnua.Trim().CompareTo("611") == 0) && (item.Kamut_rashit > 0))
                        {
                            if (bIsFirst)
                            {
                                tr.Leaka2 = item.Atzva.Substring(0, 3);
                                tr.DateKnisa2 = item.D_erua;
                                tr.DaysLeft2 = item.Yamim_notar;
                                tr.MlayEggs2 = item.Kamut_rashit;
                                bIsFirst = false; 
                            }
                            else
                            {
                                if (item.D_erua.CompareTo(tr.DateKnisa2) == 0)
                                {
                                    tr.MlayEggs2 += item.Kamut_rashit;
                                }
                                else
                                {
                                    tr.IsSingal2 = false;
                                }
                            }
                        }
                    }
                    if (bIsFirst)
                        tr.IsSingal2 = false;
                }
                else if (j == 3)
                {
                    tr.Valid3 = true;
                    tr.StoreRoomName3 = li[i].Teur;
                    tr.StoreRoomCode3 = li[i].Machsan;
                    tr.StoreRoomKibolet3 = li[i].Kibolet;
                    tr.StoreRoomInventory3 = inventoryRepository.getInventoryToStoreroom(stToken, li[i].Machsan).Answer;
                    tr.StoreRoomNumYamim3 = li[i].Ifyun_dec_1;
                    tr.Type3 = li[i].Sug_shiyuch;
                    lTm = NextGenerationServer.BLL.AccessBLL.Default.Inventory.getTnoutMlaiToMachsan(stToken, li[i].Machsan);
                    tr.IsSingal3 = true;
                    bIsFirst = true; 
                    foreach (TnoutMlai item in lTm)
                    {
                        if ((item.Kod_tnua.Trim().CompareTo("611") == 0) && (item.Kamut_rashit > 0))
                        {
                            if (bIsFirst)
                            {
                                tr.Leaka3 = item.Atzva.Substring(0, 3);
                                tr.DateKnisa3 = item.D_erua;
                                tr.DaysLeft3 = item.Yamim_notar;
                                tr.MlayEggs3 = item.Kamut_rashit;
                                bIsFirst = false; 
                            }
                            else
                            {
                                if (item.D_erua.CompareTo(tr.DateKnisa3) == 0)
                                {
                                    tr.MlayEggs3 += item.Kamut_rashit;
                                }
                                else
                                {
                                    tr.IsSingal3 = false;
                                }
                            }
                        }
                    }
                    if (bIsFirst)
                        tr.IsSingal3 = false;
                }
                else if (j == 4)
                {
                    tr.Valid4 = true;
                    tr.StoreRoomName4 = li[i].Teur;
                    tr.StoreRoomCode4 = li[i].Machsan;
                    tr.StoreRoomKibolet4 = li[i].Kibolet;
                    tr.StoreRoomInventory4 = inventoryRepository.getInventoryToStoreroom(stToken, li[i].Machsan).Answer;
                    tr.StoreRoomNumYamim4 = li[i].Ifyun_dec_1;
                    tr.Type4 = li[i].Sug_shiyuch;
                    lTm = NextGenerationServer.BLL.AccessBLL.Default.Inventory.getTnoutMlaiToMachsan(stToken, li[i].Machsan);
                    tr.IsSingal4 = true;
                    bIsFirst = true; 
                    foreach (TnoutMlai item in lTm)
                    {
                        if ((item.Kod_tnua.Trim().CompareTo("611") == 0) && (item.Kamut_rashit > 0))
                        {
                            if (bIsFirst)
                            {
                                tr.Leaka4 = item.Atzva.Substring(0, 3);
                                tr.DateKnisa4 = item.D_erua;
                                tr.DaysLeft4 = item.Yamim_notar;
                                tr.MlayEggs4 = item.Kamut_rashit;
                                bIsFirst = false; 
                            }
                            else
                            {
                                if (item.D_erua.CompareTo(tr.DateKnisa4) == 0)
                                {
                                    tr.MlayEggs4 += item.Kamut_rashit;
                                }
                                else
                                {
                                    tr.IsSingal4 = false;
                                }
                            }
                        }
                    }
                    if (bIsFirst)
                        tr.IsSingal4 = false;
                }
                else if (j == 5)
                {
                    tr.Valid5 = true;
                    tr.StoreRoomName5 = li[i].Teur;
                    tr.StoreRoomCode5 = li[i].Machsan;
                    tr.StoreRoomKibolet5 = li[i].Kibolet;
                    tr.StoreRoomInventory5 = inventoryRepository.getInventoryToStoreroom(stToken, li[i].Machsan).Answer;
                    tr.StoreRoomNumYamim5 = li[i].Ifyun_dec_1;
                    tr.Type5 = li[i].Sug_shiyuch;
                    lTm = NextGenerationServer.BLL.AccessBLL.Default.Inventory.getTnoutMlaiToMachsan(stToken, li[i].Machsan);
                    tr.IsSingal5 = true;
                    bIsFirst = true; 
                    foreach (TnoutMlai item in lTm)
                    {
                        if ((item.Kod_tnua.Trim().CompareTo("611") == 0) && (item.Kamut_rashit > 0))
                        {
                            if (bIsFirst)
                            {
                                tr.Leaka5 = item.Atzva.Substring(0, 3);
                                tr.DateKnisa5 = item.D_erua;
                                tr.DaysLeft5 = item.Yamim_notar;
                                tr.MlayEggs5 = item.Kamut_rashit;
                                bIsFirst = false; 
                            }
                            else
                            {
                                if (item.D_erua.CompareTo(tr.DateKnisa5) == 0)
                                {
                                    tr.MlayEggs5 += item.Kamut_rashit;
                                }
                                else
                                {
                                    tr.IsSingal5 = false;
                                }
                            }
                        }
                    }
                    if (bIsFirst)
                        tr.IsSingal5 = false;
                }
                else if (j == 6)
                {
                    tr.Valid6 = true;
                    tr.StoreRoomName6 = li[i].Teur;
                    tr.StoreRoomCode6 = li[i].Machsan;
                    tr.StoreRoomKibolet6 = li[i].Kibolet;
                    tr.StoreRoomInventory6 = inventoryRepository.getInventoryToStoreroom(stToken, li[i].Machsan).Answer;
                    tr.StoreRoomNumYamim6 = li[i].Ifyun_dec_1;
                    tr.Type6 = li[i].Sug_shiyuch;
                    lTm = NextGenerationServer.BLL.AccessBLL.Default.Inventory.getTnoutMlaiToMachsan(stToken, li[i].Machsan);
                    tr.IsSingal6 = true;
                    bIsFirst = true; 
                    foreach (TnoutMlai item in lTm)
                    {
                        if ((item.Kod_tnua.Trim().CompareTo("611") == 0) && (item.Kamut_rashit > 0))
                        {
                            if (bIsFirst)
                            {
                                tr.Leaka6 = item.Atzva.Substring(0, 3);
                                tr.DateKnisa6 = item.D_erua;
                                tr.DaysLeft6 = item.Yamim_notar;
                                tr.MlayEggs6 = item.Kamut_rashit;
                                bIsFirst = false; 
                            }
                            else
                            {
                                if (item.D_erua.CompareTo(tr.DateKnisa6) == 0)
                                {
                                    tr.MlayEggs6 += item.Kamut_rashit;
                                }
                                else
                                {
                                    tr.IsSingal6 = false;
                                }
                            }
                        }
                    }
                    if (bIsFirst)
                        tr.IsSingal6 = false;
                }
                else if (j == 7)
                {
                    tr.Valid7 = true;
                    tr.StoreRoomName7 = li[i].Teur;
                    tr.StoreRoomCode7 = li[i].Machsan;
                    tr.StoreRoomKibolet7 = li[i].Kibolet;
                    tr.StoreRoomInventory7 = inventoryRepository.getInventoryToStoreroom(stToken, li[i].Machsan).Answer;
                    tr.StoreRoomNumYamim7 = li[i].Ifyun_dec_1;
                    tr.Type7 = li[i].Sug_shiyuch;
                    lTm = NextGenerationServer.BLL.AccessBLL.Default.Inventory.getTnoutMlaiToMachsan(stToken, li[i].Machsan);
                    tr.IsSingal7 = true;
                    bIsFirst = true; 
                    foreach (TnoutMlai item in lTm)
                    {
                        if ((item.Kod_tnua.Trim().CompareTo("611") == 0) && (item.Kamut_rashit > 0))
                        {
                            if (bIsFirst)
                            {
                                tr.Leaka7 = item.Atzva.Substring(0, 3);
                                tr.DateKnisa7 = item.D_erua;
                                tr.DaysLeft7 = item.Yamim_notar;
                                tr.MlayEggs7 = item.Kamut_rashit;
                                bIsFirst = false; 
                            }
                            else
                            {
                                if (item.D_erua.CompareTo(tr.DateKnisa7) == 0)
                                {
                                    tr.MlayEggs7 += item.Kamut_rashit;
                                }
                                else
                                {
                                    tr.IsSingal7 = false;
                                }
                            }
                        }
                    }
                    if (bIsFirst)
                        tr.IsSingal7 = false;
                }
                else if (j == 8)
                {
                    tr.Valid8 = true;
                    tr.StoreRoomName8 = li[i].Teur;
                    tr.StoreRoomCode8 = li[i].Machsan;
                    tr.StoreRoomKibolet8 = li[i].Kibolet;
                    tr.StoreRoomInventory8 = inventoryRepository.getInventoryToStoreroom(stToken, li[i].Machsan).Answer;
                    tr.StoreRoomNumYamim8 = li[i].Ifyun_dec_1;
                    tr.Type8 = li[i].Sug_shiyuch;
                    lTm = NextGenerationServer.BLL.AccessBLL.Default.Inventory.getTnoutMlaiToMachsan(stToken, li[i].Machsan);
                    tr.IsSingal8 = true;
                    bIsFirst = true; 
                    foreach (TnoutMlai item in lTm)
                    {
                        if ((item.Kod_tnua.Trim().CompareTo("611") == 0) && (item.Kamut_rashit > 0))
                        {
                            if (bIsFirst)
                            {
                                tr.Leaka8 = item.Atzva.Substring(0, 3);
                                tr.DateKnisa8 = item.D_erua;
                                tr.DaysLeft8 = item.Yamim_notar;
                                tr.MlayEggs8 = item.Kamut_rashit;
                                bIsFirst = false; 
                            }
                            else
                            {
                                if (item.D_erua.CompareTo(tr.DateKnisa8) == 0)
                                {
                                    tr.MlayEggs8 += item.Kamut_rashit;
                                }
                                else
                                {
                                    tr.IsSingal8 = false;
                                }
                            }
                        }
                    }
                    if (bIsFirst)
                        tr.IsSingal8 = false;
                    lt.Add(tr);
                }

            }
            if (j != 8)
                lt.Add(tr);

            return lt;
        }
    }
}
