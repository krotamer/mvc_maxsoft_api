using System.Web.Http;
using System.Web.Mvc;

namespace MvcNextGenertionApi.Areas.HelpPage
{
    /// <summary>
    /// HelpPageAreaRegistration class
    /// </summary>
    public class HelpPageAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HelpPage";
            }
        }

        /// <summary>
        /// RegisterArea void function
        /// </summary>
        /// <param name="context">AreaRegistrationContext</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HelpPage_Default",
                //"Help/{action}/{apiId}",
                "{action}/{apiId}",
                new { controller = "Help", action = "Index", apiId = UrlParameter.Optional });

            HelpPageConfig.Register(GlobalConfiguration.Configuration);
        }
    }
}