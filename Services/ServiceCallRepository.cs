﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using MvcNextGenertionApi.Models;
using NextGenerationServer.Entities;
namespace MvcNextGenertionApi.Services
{
    /// <summary>
    /// Service Call functions
    /// </summary>
    public class ServiceCallRepository
    {

        /// <summary>
        /// Function to get list of service Calls
        /// </summary>
        /// <param name="stToken">Token key</param>
        /// <param name="stServiceCallId">Service Call Id -> moked - kriat_sherut</param>
        /// <param name="liWhere">Where List</param>
        /// <returns>List of ServiceCall Model</returns>
        public List<NextGenerationServer.Entities.ServiceCall> getServiceCall(string stToken, string stServiceCallId, List<KeyValuePair<string, object>> liWhere)
        {
            try
            {
                List<NextGenerationServer.Entities.ServiceCall> serviceCalls = NextGenerationServer.BLL.AccessBLL.Default.ServiceCall.getServiceCall(stToken, stServiceCallId, liWhere);
                
                return serviceCalls;
            }
            catch (Exception)
            {
                return null;
            }


        }
        
        /// <summary>
        /// Function to get Care Description Iturit DataSourse
        /// </summary>
        /// <param name="stToken">Token key</param>
        /// <returns>IturitDataSource list</returns>
        public List<IturitDataSource> getCareDescriptionIturitDataSourse(string stToken)
        {
            return NextGenerationServer.BLL.AccessBLL.Default.ServiceCall.getCareDescriptionIturitDataSourse(stToken);
        }

        /// <summary>
        /// Function to insert ServiceCall Treatment
        /// </summary>
        /// <param name="stToken">Token key</param>
        /// <param name="liServiceCallTreatment">ServiceCall Treatment List</param>
        /// <param name="liServiceCallItems">ServiceCall Items List</param>
        /// <param name="liServiceCallItemsPhoto">ServiceCall Items Photo List</param>
        /// <param name="serviceCallSignature">serviceCall Signature Entitie</param>
        /// <returns>ReturnAnswer Entitie</returns>
        public ReturnAnswer insertServiceCallTreatment(string stToken, List<ServiceCallTreatment> liServiceCallTreatment, List<ServiceCallItems> liServiceCallItems, List<ServiceCallItemsPhoto> liServiceCallItemsPhoto, ServiceCallSignature serviceCallSignature)
        {
            return NextGenerationServer.BLL.AccessBLL.Default.ServiceCall.insertServiceCallTreatment(stToken, liServiceCallTreatment, liServiceCallItems, liServiceCallItemsPhoto, serviceCallSignature);
        }

    }
}