﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using MvcNextGenertionApi.Models;
using NextGenerationServer.Entities;

namespace MvcNextGenertionApi.Services
{
    /// <summary>
    /// Login Functions
    /// </summary>
    public class LoginRepository
    {
        /// <summary>
        /// Function To Check User By Token
        /// </summary>
        /// <param name="stToken">Token key</param>
        /// <returns>User Model</returns>
        public User checkUser(string stToken)
        {
            try
            {
                NextGenerationServer.Entities.User user = NextGenerationServer.BLL.AccessBLL.Default.Login.checkIfUserValid(stToken);

                return new User { IsValid = user.IsValid, Name = user.Name, IsAdmin = user.IsAdmin, MustEnterNewPassword = false };
            }
            catch (Exception)
            {
                return new User { IsValid = false, Message = "User Not Defined !!!", MustEnterNewPassword = false };
            }
            
        
        }

        /// <summary>
        /// Function to check User Validation
        /// </summary>
        /// <param name="stUserName">User Name</param>
        /// <param name="stPwd">Password</param>
        /// <returns>User Model</returns>
        public User checkUser(string stUserName, string stPwd)
        {
            return NextGenerationServer.BLL.AccessBLL.Default.Login.checkIfUserValid(stUserName, stPwd);
        
        }

        /// <summary>
        /// Function To Check If User Valid
        /// </summary>
        /// <param name="login">Login Model</param>
        /// <returns>User Model</returns>
        public User checkUser(Login login)
        {
            return NextGenerationServer.BLL.AccessBLL.Default.Login.checkIfUserValid(login.UserName, login.Password);

        }

        /// <summary>
        /// function to change password
        /// </summary>
        /// <param name="newPassword">newPassword model</param>
        /// <returns>ReturnAnswer model</returns>
        public ReturnAnswer changePassword(NewPassword newPassword)
        {
            return NextGenerationServer.BLL.AccessBLL.Default.Login.changePassword(newPassword.UserName, newPassword.OldPwd, newPassword.NewPwd); 
        }

        /// <summary>
        /// function for reset Password
        /// </summary>
        /// <param name="resetPwd">ResetPassword model</param>
        /// <returns>ReturnAnswer model</returns>
        public ReturnAnswer resetPassword(ResetPassword resetPwd)
        {
            return NextGenerationServer.BLL.AccessBLL.Default.Login.resetPassword(resetPwd.UserName.Trim());
        }
        
    }
}