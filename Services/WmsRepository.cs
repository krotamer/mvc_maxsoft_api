﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcNextGenertionApi.Models;
using NextGenerationServer.Entities;

namespace MvcNextGenertionApi.Services
{
    public class WmsRepository
    {
        public List<Est169> getOrdersToShipment(string stToken, string stShipment)
        {
            try
            {
                return NextGenerationServer.BLL.AccessBLL.Default.Wms.getOrdersToShipment(stToken, stShipment);
            }
            catch (Exception)
            {
                return new List<Est169>();
            }
        }

        public Est169 getOrdersDetails(string stToken, string stOrderMoked, string stOrderTeuda)
        {
            try
            {
                return NextGenerationServer.BLL.AccessBLL.Default.Wms.getOrdersDetails(stToken, stOrderMoked, stOrderTeuda);
            }
            catch (Exception)
            {
                return new Est169();
            }
        }

        public bool rePrintTeudatMishloach(string stToken, string stOrderMoked, string stOrderTeuda)
        {
            try
            {
                return NextGenerationServer.BLL.AccessBLL.Default.Wms.rePrintTeudatMishloach(stToken, stOrderMoked, stOrderTeuda);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool rePrintTeudatMishloachForShipment(string stToken, string stShipment)
        {
            try
            {
                return NextGenerationServer.BLL.AccessBLL.Default.Wms.rePrintTeudatMishloachForShipment(stToken,stShipment);
            }
            catch (Exception)
            {
                return false;
            }
        }



    }
}