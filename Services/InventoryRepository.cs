﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NextGenerationServer.Entities;

namespace MvcNextGenertionApi.Services
{
    /// <summary>
    /// Inventory Functions
    /// </summary>
    public class InventoryRepository
    {

        /// <summary>
        /// Function to get Inventory to item
        /// </summary>
        /// <param name="stToken">User Token</param>
        /// <param name="stItem">Parit</param>
        /// <returns>Inventory</returns>
        public ReturnDoubleAnswer getInventoryToItem(string stToken, string stItem)
        {
            try
            {
                return NextGenerationServer.BLL.AccessBLL.Default.Inventory.getInventoryForItem(stToken, stItem);
            }
            catch (Exception ex)
            {
                return new ReturnDoubleAnswer(double.NaN, ex.Message);
            }
        }

        /// <summary>
        /// Function to get Inventory to storeroom
        /// </summary>
        /// <param name="stToken">User Token</param>
        /// <param name="stStoreroom">Machsan</param>
        /// <returns>Inventory</returns>
        public ReturnDoubleAnswer getInventoryToStoreroom(string stToken, string stStoreroom)
        {
            try
            {
                return NextGenerationServer.BLL.AccessBLL.Default.Inventory.getInventoryForStoreRoom(stToken, stStoreroom);
            }
            catch (Exception ex)
            {
                return new ReturnDoubleAnswer(double.NaN, ex.Message);
            }
        }

        /// <summary>
        /// function to get items in storerrom and the inventory
        /// </summary>
        /// <param name="stToken">User Token</param>
        /// <param name="stStoreroom">Machsan</param>
        /// <param name="bIncludeItur">Include Itur</param>
        /// <param name="bIncludeAtzva">Include Atzva</param>
        /// <returns>item list</returns>
        public List<Inventory> getInventoryListToStoreroom(string stToken, string stStoreroom, bool bIncludeItur, bool bIncludeAtzva)
        {
            try
            {
                return NextGenerationServer.BLL.AccessBLL.Default.Inventory.getItemsInStoreRoom(stToken, stStoreroom, bIncludeItur, bIncludeAtzva);
            }
            catch (Exception)
            {
                return new List<Inventory>();
            }
        }

        /// <summary>
        /// function to get tnout mlai list to storeroom
        /// </summary>
        /// <param name="stToken">User Token</param>
        /// <param name="stStoreroom">Machsan</param>
        /// <returns>Tnout Mlai</returns>
        public List<TnoutMlai> getTnoutMlaiToMachsan(string stToken, string stStoreroom)
        {
            try
            {
                return NextGenerationServer.BLL.AccessBLL.Default.Inventory.getTnoutMlaiToMachsan(stToken, stStoreroom);
            }
            catch (Exception)
            {
                return new List<TnoutMlai>();
            }
        }
    }
}