﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using NextGenerationServer.Entities;

namespace MvcNextGenertionApi.Services
{
    /// <summary>
    /// Machine Functions
    /// </summary>
    public class MachineRepository
    {
        /// <summary>
        /// Function to get Machine List 
        /// </summary>
        /// <param name="stToken">Token Key</param>
        /// <param name="stMachineId">Machine Id</param>
        /// <returns>List of Machine</returns>
        public List<Machine> getMachineList(string stToken)
        {
            try
            {
                return NextGenerationServer.BLL.AccessBLL.Default.Machine.getMachineList(stToken);                
            }
            catch (Exception)
            {
                return new List<Machine>();
            }
        }
        
    }
}