﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NextGenerationServer.Entities;

namespace MvcNextGenertionApi.Services
{
    /// <summary>
    /// Storeroom Functions
    /// </summary>
    public class StoreroomRepository
    {
        /// <summary>
        /// Function To Get The List Of Storeroom by token
        /// </summary>
        /// <param name="stToken">User token</param>
        /// <returns>storeroom list</returns>
        public List<StoreRoom> getListOfStorerooms(string stToken)
        {
            try
            {
                return NextGenerationServer.BLL.AccessBLL.Default.StoreRoom.getStoreRoomList(stToken);
            }
            catch (Exception)
            {
                return new List<StoreRoom>();
            }
        }

        /// <summary>
        /// Function To Get The List Of Storeroom by token and order by
        /// </summary>
        /// <param name="stToken">User token</param>
        /// <param name="stOrderBy">Order By columns</param>
        /// <returns>storeroom list</returns>
        public List<StoreRoom> getListOfStorerooms(string stToken,string stOrderBy)
        {
            try
            {
                return NextGenerationServer.BLL.AccessBLL.Default.StoreRoom.getStoreRoomList(stToken,stOrderBy);
            }
            catch (Exception)
            {
                return new List<StoreRoom>();
            }
        }
    }
}