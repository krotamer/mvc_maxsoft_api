﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcNextGenertionApi.Models
{
    /// <summary>
    /// Class Login
    /// </summary>
    public class Login
    {
        /// <summary>
        /// User Name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// User Password
        /// </summary>
        public string Password { get; set; }        
        
    }
}