﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcNextGenertionApi.Models
{
    /// <summary>
    /// New Password Class
    /// </summary>
    public class NewPassword
    {
        /// <summary>
        /// User Name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Old Password
        /// </summary>
        public string OldPwd { get; set; }

        /// <summary>
        /// New Password
        /// </summary>
        public string NewPwd { get; set; }
    }
}