﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcNextGenertionApi.Models
{
    /// <summary>
    /// Reset Password Class
    /// </summary>
    public class ResetPassword
    {
        /// <summary>
        /// User Name
        /// </summary>
        public string UserName { get; set; }
    }
}