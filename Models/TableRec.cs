﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcNextGenertionApi.Models
{
    public class TableRec
    {
        public bool Valid1 { get; set; }
        public string StoreRoomName1 { get; set; }
        public double StoreRoomKibolet1 { get; set; }
        public double StoreRoomInventory1 { get; set; }
        public string StoreRoomCode1 { get; set; }
        public double StoreRoomNumYamim1 { get; set; }
        public string Type1 { get; set; }
        public bool IsSingal1 { get; set; }
        public DateTime DateKnisa1 { get; set; }
        public string Leaka1 { get; set; }
        public double MlayEggs1 { get; set; }
        public int DaysLeft1 { get; set; }
        public bool Valid2 { get; set; }
        public string StoreRoomName2 { get; set; }
        public double StoreRoomKibolet2 { get; set; }
        public double StoreRoomInventory2 { get; set; }
        public string StoreRoomCode2 { get; set; }
        public double StoreRoomNumYamim2 { get; set; }
        public string Type2 { get; set; }
        public bool IsSingal2 { get; set; }
        public DateTime DateKnisa2 { get; set; }
        public string Leaka2 { get; set; }
        public double MlayEggs2 { get; set; }
        public int DaysLeft2 { get; set; }
        public bool Valid3 { get; set; }
        public string StoreRoomName3 { get; set; }
        public double StoreRoomKibolet3 { get; set; }
        public double StoreRoomInventory3 { get; set; }
        public string StoreRoomCode3 { get; set; }
        public double StoreRoomNumYamim3 { get; set; }
        public string Type3 { get; set; }
        public bool IsSingal3 { get; set; }
        public DateTime DateKnisa3 { get; set; }
        public string Leaka3 { get; set; }
        public double MlayEggs3 { get; set; }
        public int DaysLeft3 { get; set; }
        public bool Valid4 { get; set; }
        public string StoreRoomName4 { get; set; }
        public double StoreRoomKibolet4 { get; set; }
        public double StoreRoomInventory4 { get; set; }
        public string StoreRoomCode4 { get; set; }
        public double StoreRoomNumYamim4 { get; set; }
        public string Type4 { get; set; }
        public bool IsSingal4 { get; set; }
        public DateTime DateKnisa4 { get; set; }
        public string Leaka4 { get; set; }
        public double MlayEggs4 { get; set; }
        public int DaysLeft4 { get; set; }
        public bool Valid5 { get; set; }
        public string StoreRoomName5 { get; set; }
        public double StoreRoomKibolet5 { get; set; }
        public double StoreRoomInventory5 { get; set; }
        public string StoreRoomCode5 { get; set; }
        public double StoreRoomNumYamim5 { get; set; }
        public string Type5 { get; set; }
        public bool IsSingal5 { get; set; }
        public DateTime DateKnisa5 { get; set; }
        public string Leaka5 { get; set; }
        public double MlayEggs5 { get; set; }
        public int DaysLeft5 { get; set; }
        public bool Valid6 { get; set; }
        public string StoreRoomName6 { get; set; }
        public double StoreRoomKibolet6 { get; set; }
        public double StoreRoomInventory6 { get; set; }
        public string StoreRoomCode6 { get; set; }
        public double StoreRoomNumYamim6 { get; set; }
        public string Type6 { get; set; }
        public bool IsSingal6 { get; set; }
        public DateTime DateKnisa6 { get; set; }
        public string Leaka6 { get; set; }
        public double MlayEggs6 { get; set; }
        public int DaysLeft6 { get; set; }
        public bool Valid7 { get; set; }
        public string StoreRoomName7 { get; set; }
        public double StoreRoomKibolet7 { get; set; }
        public double StoreRoomInventory7 { get; set; }
        public string StoreRoomCode7 { get; set; }
        public double StoreRoomNumYamim7 { get; set; }
        public string Type7 { get; set; }
        public bool IsSingal7 { get; set; }
        public DateTime DateKnisa7 { get; set; }
        public string Leaka7 { get; set; }
        public double MlayEggs7 { get; set; }
        public int DaysLeft7 { get; set; }
        public bool Valid8 { get; set; }
        public string StoreRoomName8 { get; set; }
        public double StoreRoomKibolet8 { get; set; }
        public double StoreRoomInventory8 { get; set; }
        public string StoreRoomCode8 { get; set; }
        public double StoreRoomNumYamim8 { get; set; }
        public string Type8 {get; set; }
        public bool IsSingal8 { get; set; }
        public DateTime DateKnisa8 { get; set; }
        public string Leaka8 { get; set; }
        public double MlayEggs8 { get; set; }
        public int DaysLeft8 { get; set; }
    }
}