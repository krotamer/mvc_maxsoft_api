﻿using System.Web;
using System.Web.Mvc;

namespace MvcNextGenertionApi
{
    /// <summary>
    /// FilterConfig Class
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// RegisterGlobalFilters void function
        /// </summary>
        /// <param name="filters">GlobalFilterCollection</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}